package org.example.mapper;

import org.example.bean.User;

public interface UserMapper {
    User getUserById(String id);
}
