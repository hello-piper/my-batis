package org.example.sqlsession;

public interface Excutor {
    <T> T query(String statement, Object parameter);
}
