package org.example.sqlsession;

import java.lang.reflect.Proxy;

public class MySqlsession {
    private Excutor excutor = new MyExcutor();

    private MyConfiguration configuration = new MyConfiguration();

    public <T> T selectOne(String statement, String parameter) {
        return excutor.query(statement, parameter);
    }

    public <T> T getMapper(Class<T> clz) {
        return (T) Proxy.newProxyInstance(clz.getClassLoader(), new Class[]{clz}, new MyMapperProxy(this, configuration));
    }

}
