package org.example;

import static org.junit.Assert.assertTrue;

import org.example.bean.User;
import org.example.mapper.UserMapper;
import org.example.sqlsession.MySqlsession;
import org.junit.Test;

public class MybatisTest {
    @Test
    public void test() {
        MySqlsession sqlSession = new MySqlsession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        User user = mapper.getUserById("1");
        assertTrue(user != null);
        System.out.println(user);
    }
}
